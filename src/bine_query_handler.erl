-module(bine_query_handler).

-author(kuenishi).

-include("bine_dns.hrl").

-export([start_link/4, query_handler/4]).

%% gen_server callbacks
%% -export([
%% 	 init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
%% 	 code_change/3
%% 	]).

-type socket() :: any().

-spec query_handler( socket() , tuple(), integer(), binary() ) -> atom().

query_handler(Socket, IP, InPortNo, Packet)->
    {ok, Request} = inet_dns:decode(Packet),   %   erlang:localtime(), 
%%    io:format("request: from ~p:~p ~n ~p~n", [IP, InPortNo, Request]),
    AnLists=lists:map( fun(Q)-> make_anlist(Q) end, Request#dns_rec.qdlist),
    AnList=lists:flatten( AnLists ),
    OldHeader = Request#dns_rec.header,
    Reply =#dns_rec{ header=OldHeader#dns_header{qr=1,ra=1},
		     qdlist=Request#dns_rec.qdlist,
		     anlist=AnList },
%%    io:format("reply: ~p~n", [Reply]),
    {ok,ReplyBin}=inet_dns:encode(Reply),
    gen_udp:send(Socket, IP, InPortNo, ReplyBin ).

-spec start_link( socket(), tuple(), integer(), binary() )-> pid().

start_link(Socket, IP, InPortNo, Packet)->
    spawn( bine_query_handler, query_handler, [Socket, IP, InPortNo, Packet] ).

-spec make_anlist(Query :: #dns_query{}) -> [ hostname() ] | [ ip_address() ].

make_anlist(Query=#dns_query{type=a,class=in})->
  %  io:format("~p~n", [Query#dns_query.type]),
    Addrs=get_addrs(Query#dns_query.domain),
%    io:format("make_anlist - ~p~n",[Addrs]),
    lists:map( fun(Addr)-> 
		       #dns_rr{domain=Query#dns_query.domain,
			       class=Query#dns_query.class,
			       type=Query#dns_query.type,
			       tm=undefined,
			       ttl=38,
			       data=Addr}
	       end,
	       Addrs );
make_anlist(Query=#dns_query{type=ptr,class=in})->
    {ok,Hostname}=inet:gethostname(),
    {ok,DNS_SERVER}=application:get_env(dns),
    _Reply=dns_client:ask_in_addr(Query#dns_query.domain, DNS_SERVER),
%%    io:format("make_anlist - ~p~n",[[Reply, DNS_SERVER]]),
    #dns_rr{
             domain=Query#dns_query.domain,
	     class=Query#dns_query.class,
	     type=Query#dns_query.type,
	     tm=undefined,
	     ttl=38,
	     data=Hostname
	    };
make_anlist(_) -> [].

-spec get_addrs(Domain :: hostname() ) -> [ip_address()] | [].
get_addrs(Domain) when is_list(Domain) ->
   %    [{192,168,0,72}].
    case merle:getkey(Domain) of
 	undefined ->
	    {ok,DNS_SERVER}=application:get_env(dns),
	    Reply=dns_client:ask(Domain, DNS_SERVER), %% cache the whole result, got from parent DNS.
%%	    io:format("dns_client:ask() -> ~p ~n", [Reply]),
	    case Reply of
		_ when is_record(Reply, dns_rec)->
		    Head=hd(Reply#dns_rec.anlist),
		    IPList=lists:map(fun(Ans) when is_record(Ans, dns_rr)-> Ans#dns_rr.data end,
				     Reply#dns_rec.anlist),
		    merle:set(Domain, random:uniform(65535), Head#dns_rr.ttl, IPList), %% TTLの単位
%%		    io:format(" -- by DNS: ~p -- ~n", [IPList]),
		    IPList;
		_ -> error
	    end;
	IPList when is_list(IPList) -> 
%%	    io:format(" -- by memcached: ~p -- ~n", [IPList]),
	    IPList;
	_ -> []
    end.
    
