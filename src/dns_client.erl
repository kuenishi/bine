-module(dns_client).

-include("bine_dns.hrl").

-define(DNS_PORT, 53).

-export([
	 ask/1, ask_in_addr/1,
	 ask/2, ask_in_addr/2
	 ]).

-spec ask(hostname(), ip_address()) ->  #dns_rec{} | error.
    
ask(Domain, DNSIP) when is_list(Domain) and is_tuple(DNSIP) ->
    Id = random:uniform(65535),
    Req=#dns_rec{ 
      %header=#dns_header{id=Id,rd=1,ra=0},% 
      header={dns_header,Id,0,0,0,0,1,0,0,0},
      qdlist=[{dns_query,Domain,a,in}]
     },
    {ok,Bin}=inet_dns:encode(Req),
    {ok,S}=gen_udp:open(0),
    gen_udp:send(S, DNSIP,?DNS_PORT,Bin),
    receive
	{udp, S, _IP, _Port, Data}->
	    {ok, Reply}=inet_dns:decode(Data),
%	    io:format("~p : ~p~n", [Domain, Reply]),
	    Reply
    after 1000->
	    error
    end;
ask(IP, DNSIP) when is_tuple(IP) and is_tuple(DNSIP)-> %when is_ip(IP) ->
    {A,B,C,D}=IP,
    InAddr=integer_to_list(D)++"."
	++integer_to_list(C)++"."
	++integer_to_list(B)++"."
	++integer_to_list(A)++".in-addr.arpa",
    ask_in_addr(InAddr, DNSIP);
ask(_,_) ->
    error.

ask(Domain) when is_list(Domain) -> 
    ask(Domain, {192,168,0,1});
ask(IP) when is_tuple(IP)-> %when is_ip(IP) ->
    ask(IP, {192,168,0,1});
ask(_) ->
    error.

-spec ask_in_addr(ip_address(), ip_address()) ->  #dns_rec{} | error.
ask_in_addr(InAddr, DNSIP) when is_list(InAddr) and is_tuple(DNSIP)-> %when is_ip(IP) ->
    Id = random:uniform(65535),
    Req=#dns_rec{
%      header=#dns_header{id=Id,rd=1,ra=0},
      header={dns_header,Id,0,0,0,0,1,0,0,0},
      qdlist=[ #dns_query{domain=InAddr,type=ptr,class=in} ]
     },
    {ok, Bin}=inet_dns:encode(Req),
    {ok,S}=gen_udp:open(0),
    gen_udp:send(S,DNSIP,?DNS_PORT,Bin),
    receive
	{udp, S, _IP, _Port, Data}->
	    {ok, Reply}=inet_dns:decode(Data),
%	    io:format("~p : ~p~n", [InAddr, Reply]),
%	    FQDNList=lists:map( fun(DNS_RR)-> DNS_RR#dns_rr.data end, 
	    Reply
    after 1000->
	    error
    end.
    
ask_in_addr(InAddr)->
    ask_in_addr(InAddr, {192,168,0,1}).
    
