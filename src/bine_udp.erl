-module(bine_udp).
-author(kuenishi).

-behaviour(gen_server).

-include("bine_dns.hrl").

-export([start_link/0]).

%% gen_server callbacks
-export([
	 init/1, handle_call/3, handle_cast/2, handle_info/2, terminate/2,
	 code_change/3
	]).

-type socket() :: any().

-record( state,
	 {socket :: socket(),
	  host   :: hostname(),
	  port   :: pos_integer()
	 }).

start_link()->
    {ok,Host}=inet:gethostname(),
    {ok,Port}=application:get_env(port),
    io:format("starting UDP with port ~p.~n", [Port]),
    gen_server:start_link(?MODULE, [Host, Port], [{debug,[trace]}]).

%-spec init(list()) -> {ok, pid()}.
-spec init( list() ) -> {ok, #state{}}.
init([Host, Port])->
    case gen_udp:open(Port, [{active, once}]) of
	{ok, Socket} -> %, [inet, binary, {active, false}]),
	    {ok, #state{socket=Socket,host=Host,port=Port}};
	Other ->
	    error_logger:error_msg("can't open port ~p: ~p. Is really memcached working at ~p?~n", 
				   [Port, Other, Host]),
	    {stop, econnrefused}
    end.

handle_call(_, _From, Socket)->
    {reply, Socket, Socket}.

handle_cast(stop, State)->
    {stop, normal, State}.

-spec handle_info( {udp, Socket :: socket(), IP :: ip_address(), InPortNo :: pos_integer(), binary() },
		   #state{} ) -> {noreply, #state{}}.
handle_info({udp, Socket, IP, InPortNo, Packet}, State)->
    bine_query_handler:start_link(Socket,IP,InPortNo,Packet),
    inet:setopts(Socket, [{active,once}]),
    {noreply, State};
handle_info(_, State)->
    {noreply, State}.

-spec terminate( normal , pid() )-> 'ok'.
terminate(normal, Pid)->
    Pid ! {close, normal},
    ok.

code_change(_, _, State) -> {ok, State}.
