-module(bine_app).
-behaviour(application).

-export([start/2, stop/1]).

start(_Type, _StartArgs)->
    %io:format("~p ~p: ~p~n", [?FILE, ?LINE, StartArgs]),
    bine_sup:start_link().

stop(_State)->
    ok.

