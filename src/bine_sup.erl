-module(bine_sup).
-behaviour(supervisor).

-export([start_link/0, init/1]).

start_link()->
    supervisor:start_link({local, ?MODULE}, ?MODULE, []).

-type policy() :: one_for_all | one_for_one.
-type childspec() :: {atom(), { module(), atom(), list()}, permanent, 
		      pos_integer(), worker, [module()]}.

-spec init( list() ) -> {ok, { policy(), pos_integer(), pos_integer()}, [childspec()] }.
init([])->
    {Host, Port} = {"localhost", 11211},
    Children = [
		{merle, {merle, connect, [Host, Port]}, permanent, 2000, worker, [merle]}
		,{bine_udp, {bine_udp, start_link, []},  permanent, 2000, worker, [bine_udp]}
%		,{bine_tcp, {bine_tcp, start_link, []},  permanent, 2000, worker, [bine_tcp]}
	       ],
    io:format("starting: ~p~n", [Children]),
    {ok, {{one_for_one, 1, 1}, Children}}.
