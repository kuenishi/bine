
# s: start
# start:
# 	erl -pa ebin -s application start 'aine' # -s init stop

# stop:

t: test
test: build

b: build
build:
	cd src; erl -make

c: clean
clean:
	-rm -rf ebin/*.beam
